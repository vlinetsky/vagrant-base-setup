class updateinst {
  exec { 'apt-get update':
    command => '/usr/bin/apt-get update',
	before  => [Package['mysql-client'], Package['mysql-server'] ],
  }
}

include updateinst