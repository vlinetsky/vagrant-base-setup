include updateinst
include midnightcommander
include motd
#include java
include apache
include apache::mod::php
include mysql
include mysql::server
include mysql::php

apache::vhost { 'vagrant.lan':
    priority        => '10',
    port            => '80',
    docroot         => '/vagrant',
}

#mysql::server { 'config_hash': 
#   root_password => 'root',
#   bind_address => '',
#}

#class { 'mysql::config':
#     root_password => 'root',
#     bind_address  => '',
#}

mysql::db { 'testvagrantdb':
    user     => 'valera_vagrant',
    password => 'valera_password',
	host => '%',
    grant    => ['all'],
	sql => '/vagrant/db.sql',
}

#firewall { '100 allow mysql from internal':
#    proto       => 'tcp',
#    dport       => '3307',
#    source      => '10.0.2.0/24',
#    action      => accept,
#}
